package it.zimbone.poc;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by cslvz1 on 27/01/2017.
 */
@ApplicationPath("/rs")
public class MyApplication extends Application {
    public MyApplication() {
    }
}
