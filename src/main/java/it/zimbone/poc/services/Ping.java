package it.zimbone.poc.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;

/**
 * Created by v.zimbone on 30/01/2017.
 */
@Path("ping")
public class Ping {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response ping() {
        return Response.ok().entity("It works! " + new Date(System.currentTimeMillis())).build();

    }
}
