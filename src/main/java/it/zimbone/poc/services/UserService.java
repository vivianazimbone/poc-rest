package it.zimbone.poc.services;

import it.zimbone.poc.entities.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;

import static org.jboss.resteasy.spi.CorsHeaders.*;

@Singleton
@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class UserService {

    private final static Log LOG = LogFactory.getLog(UserService.class);
    private HashMap<String, User> users = new HashMap<>();

    @PostConstruct
    public void init() {
        LOG.debug("init");
        User viviana = new User("viviana", "id:o2SfNW2piZAAAAAAAAAAGQ");
        users.put(viviana.getUser(), viviana);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@QueryParam("user") String user) {
        LOG.info("method=getUser , user=" + user);
        return Response.ok(users.get(user)).header(ACCESS_CONTROL_ALLOW_ORIGIN, "*").build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postUser(User user) {
        LOG.info("method=postUser, user=" + user.getUser() + " idPhoto=" + user.getPhoto());
        users.put(user.getUser(), user);
        return Response.ok().header(ACCESS_CONTROL_ALLOW_ORIGIN, "*").build();
    }


    @OPTIONS
    @Produces(MediaType.APPLICATION_JSON)
    public Response options() {
        return Response.ok()
                .header(ACCESS_CONTROL_ALLOW_ORIGIN, "*")
                .header(ACCESS_CONTROL_ALLOW_CREDENTIALS, "true")
                .header(ACCESS_CONTROL_ALLOW_METHODS, "GET, POST, OPTIONS, PUT, DELETE")
                .header(ACCESS_CONTROL_ALLOW_HEADERS, "content-type, x-requested-with, x-requested-by")
                .build();
    }
}
