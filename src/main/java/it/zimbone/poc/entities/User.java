package it.zimbone.poc.entities;

/**
 * Created by v.zimbone on 22/01/2017.
 */
public class User {

    private String user;
    private String photo;

    public User() {
    }

    public User(String user, String photo) {
        this.user = user;
        this.photo = photo;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
